﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace SetOrdersAsDelivered
{
    public class DeliveryUpdateBody
    {
        [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
        public string Status { get; set; }

        [JsonProperty("updatedDate", NullValueHandling = NullValueHandling.Ignore)]
        public string UpdatedDate { get; set; }

        [JsonProperty("active", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Active { get; set; }

        [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
        public string Message { get; set; }

    }
}
