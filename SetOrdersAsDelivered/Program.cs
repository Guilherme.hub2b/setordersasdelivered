﻿using Hub2b.DatabaseApi.Models;
using Hub2b.IntegrationEvents;
using Hub2b.IntegrationEvents.OrderEvents;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SetOrdersAsDelivered
{
    class Program
    {
        static void Main(string[] args)
        {
            string systemName = "EMillennium";
            string idTenant = "2439";

            List<string> orderNumbers = new List<string>() { "2000000188511580", "2250702900", "2261711766", "2266244714", "2266756961", "2266761213", "2268329196", "2268331316", "2269555484", "2273006525", "2529492630001-A", "2529901510001-A", "2531273530001-A", "2534492420001-A", "2534512160001-A", "2538147380001-A", "2538386100001-A", "2539760150001-A", "2540506460001-A", "2557290110001-A", "2583428060001-A", "2667026420001-A", "272576765201", "272606906102", "272628487402", "272635662101", "272645921001", "272666445102", "272670246901", "272672122701", "272694581901", "272695435301", "272696516701", "272747611001", "272750850301", "272754917601", "272756340701", "30984383", "31064889", "32674574", "34332680", "34447325", "34665030", "34954903", "351491190301", "351493840401", "35168085", "35204226", "35207747", "35216041", "35281298", "35287183", "35311411", "35390526", "35407054", "35420986", "35453627", "35455074", "35455166", "35470477", "35481745", "35510655", "35519099", "35571162", "35579609", "35681365", "35685645", "35692322", "35719561", "35731858", "35789807", "35927340", "35996713", "36022961", "36056778", "36069798", "36078737", "36092801", "36132551", "36136884", "36143142", "36176915", "36182554", "36194035", "36244901", "36250980", "36264979", "36272578", "36280943", "36282961", "36301608", "36301967", "36311972", "36312893", "36321005", "36323469", "36333950", "36336285", "36338552", "36344695", "36344721", "36359288", "36359360", "36362490", "36376358", "36386968", "36389876", "36389958", "36392903", "36396850", "36398652", "36398849", "36400009", "36402113", "36402758", "36415415", "36422113", "36428998", "36430000", "36430931", "36433306", "36434521", "36438186", "36442826", "36443974", "36445185", "36449324", "36452917", "36454907", "36458509", "36458565", "36461631", "36463145", "36470069", "36478062", "36479335", "36482194", "36488933", "36489623", "36491514", "36492976", "36493206", "36493809", "36495430", "36498225", "36502786", "36503899", "36505486", "36505868", "36505939", "36516135", "36517950", "36534071", "36540215", "36541622", "36542384", "36550655", "36551308", "36555698", "36557815", "36559158", "36559468", "36560887", "36566322", "36570038", "36570218", "36570994", "36572104", "36575502", "36577680", "36578944", "36579831", "36580404", "36580583", "36583061", "36583111", "36584171", "36586331", "36587207", "36588663", "36596615", "36599354", "36604120", "36606971", "36606996", "36611517", "36612987", "36613812", "36618229", "36618935", "36620577", "36621605", "36623846", "36625052", "36627151", "36632701", "36637009", "36638985", "36639134", "36640973", "36641219", "36643285", "36644146", "36644387", "36645795", "36645819", "36648159", "36653183", "36660394", "36660451", "36660863", "36662597", "36664323", "36667905", "36672888", "36673549", "36673897", "36719943", "36722203", "36725719", "36745286", "36745321", "36752894", "36752964", "36755505", "36755636", "36756040", "36759222", "36764891", "36766352", "36776587", "LU-7905500530398782", "LU-7911500532115246", "LU-7916500532798287", "LU-7943500535797028", "LU-7944500535878904", "LU-7944500535942708", "LU-7948500537532881", "LU-7951500539068876", "LU-7957500541283469", "LU-7958500541328339", "LU-7959500541685973", "LU-7963500541984598", "LU-7963500541986892", "LU-7964500542039392", "LU-7965500542111164", "LU-7965500542112562", "LU-7966500542155779", "LU-7966500542166903", "LU-7981500544565660", "LU-7981500544570459", "LU-7981500544593520", "LU-7982500544810361", "LU-7982500544849596", "LU-7984500545852230", "LU-7984500545937549", "LU-7984500545945031", "LU-7988500546672371", "LU-7988500546717444", "LU-7992500547641754", "LU-8004500548996081", "LU-8007500549671620", "LU-8010500550039544", "LU-8010500550099652", "LU-8015500550595546", "LU-8015500550657346", "LU-8015500550676059", "LU-8015500550677013", "LU-8015500550700944", "LU-8016500550739888" };
            var HubOrders = GetOrders(idTenant, systemName, orderNumbers);
            UpdateOrdersAsDelivered(HubOrders.Where(o => o.Status.Status == OrderStatus.Shipped).ToList());

            var NotUpdated = HubOrders.Where(o => o.Status.Status != OrderStatus.Shipped).ToList();
        }
        private static List<Order> GetOrders(string idTenant, string systemName, List<string> orderNumbers)
        {
            var orders = new List<Order>();

            foreach (var orderNumber in orderNumbers)
            {
                var client = new RestClient($"http://localhost:5001/api/Tenant/{idTenant}/Order/{systemName}/{orderNumber}");
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                if (!response.IsSuccessful)
                {
                    if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    {
                        Console.WriteLine($"{orderNumber} - Não encontrado. Pulando");
                        continue;
                    }
                    throw new Exception($"Error while get orders from HubDatabaseAPI. Status: {response.StatusCode}. {response.Content}");
                }
                orders.Add(JsonConvert.DeserializeObject<Order>(response.Content));
            }
            return orders;
        }
    
        private static void UpdateOrdersAsDelivered(List<Order> orders)
        {
            foreach(var order in orders)
            {
                try
                {
                    var client = new RestClient($"http://localhost:5001/api/Order/{order.Reference.Id}/Status");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.PUT);
                    request.AddHeader("Content-Type", "application/json");
                    request.AddParameter("application/json", GetUpdateBody(), ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    if (!response.IsSuccessful)
                        throw new Exception(response.Content);
                    Console.WriteLine($"Order {order.Reference.Source} - {response.StatusCode}");
                }
                catch (Exception e)
                {
                    Console.WriteLine($"{order.Reference.Source} - Erro");
                }
            }
        }

        private static string GetUpdateBody()
        {
            return JsonConvert.SerializeObject(new DeliveryUpdateBody()
            {
                Active = true,
                Status = "Delivered",
                Message = ""
            });
        }

    }
}
